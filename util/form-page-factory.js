const { episode } = require("./../../config");
const { getFieldCommands } = require("./form-group-fields");
const { activeXpath } = require("./../util");

module.exports = function(formGroups) {
    const sections = formGroups.reduce((p, c) => {
        return Object.assign(p, {
            [c]: require(`./../formGroupSections/formGroup${c}`)
        });
    }, {});

    const commands = [
        {
            waitForLoad() {
                return this.waitForElementVisible(
                    "@title"
                ).waitForElementNotPresent("@spinner");
            },
            fill() {
                formGroups.forEach(n => this.section[n].open().fill());
                return this.waitForElementVisible("@outreachCompletedTitle");
            },
            validate() {
                const blacklist = ["Lead", "Summary", "AgencySummary"];
                const verifyFormGroups = formGroups.filter(
                    n => blacklist.indexOf(n) === -1
                );
                verifyFormGroups.forEach(n =>
                    this.section[n].open().validate()
                );
                return this;
            },
            clickEpisodeButton() {
                return this.waitForElementVisible("@episodeButton").click(
                    "@episodeButton"
                );
            },
            clickEpisodeLink() {
                this.api.perform((client, done) => {
                    client.execute(
                        function() {
                            window.scrollTo(0, 0);
                        },
                        [],
                        done
                    );
                });
                this.waitForElementVisible("@episodeLink");
                this.click("@episodeLink");
                return this;
            },
            getOutreachId() {
                return new Promise(resolve =>
                    this.api.url(value => {
                        const parts = value.value.split("/");
                        const id = parts[parts.length - 2];
                        resolve(id);
                    })
                );
            }
        }
    ];

    return {
        commands,
        sections,
        elements: {
            title: activeXpath(
                "//p[contains(@class, 'entityNameTitle') and text()='CareShare Outreach']"
            ),
            spinner: ".cScl_cs_outreach .slds-spinner",
            episodeButton: activeXpath(`//button[text()='Return to Episode']`),
            episodeLink: activeXpath(
                `//div[contains(@class, 'slds-page-header')]//a[contains(@class, 'outputLookupLink') and text()='${episode.name}']`
            ),
            outreachCompletedTitle: activeXpath(
                "//span[text()='The Outreach has been completed']"
            )
        }
    };
};
