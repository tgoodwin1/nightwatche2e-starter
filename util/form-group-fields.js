const { activeXpathSelector } = require("./../util");

/**
 * Selectors for form group field types
 */
const formGroupSelector = {
    dateDay(value) {
        return activeXpathSelector(
            `//span[@class="slds-day" and text()="${value}"]`
        );
    },
    date(label) {
        return activeXpathSelector(
            `//label[text()="${label}"]/following-sibling::div/input`
        );
    },
    radio(label, value) {
        return activeXpathSelector(
            `//legend[text()="${label}"]/following-sibling::*//span[text()="${value}"]`
        );
    },
    text(label) {
        return activeXpathSelector(
            `//span[text()="${label}"]/../following-sibling::div/input`
        );
    },
    textarea(label) {
        return activeXpathSelector(
            `//label[text()="${label}"]/following-sibling::div/textarea`
        );
    },
    multiselectOption(label, option) {
        const selectXpath = this.multiselect(label);
        return `${selectXpath}/../following-sibling::div//span[text()="${option}"]`;
    },
    multiselect(label) {
        return activeXpathSelector(
            `//fieldset[contains(@class, 'cScl_form_multiSelect')]//span[text()="${label}"]/..`
        );
    },
    select(label) {
        return activeXpathSelector(
            `//fieldset[contains(@class, 'cScl_form_select')]//span[text()="${label}"]/..`
        );
    },
    selectOption(label, option) {
        const selectXpath = this.select(label);
        return `${selectXpath}/../following-sibling::div//span[text()="${option}"]`;
    }
};

const verifyFormGroupSelector = {
    radio(label) {
        return `//legend[text()="${label}"]/following-sibling::*//input[@checked="checked"]`;
    },
    text: formGroupSelector.text,
    textarea: formGroupSelector.textarea,
    date: formGroupSelector.date,
    select(label) {
        return `//span[text()="${label}"]/../following-sibling::div/input`;
    },
    multiselect(label) {
        return `//span[text()="${label}"]/../following-sibling::div/input`;
    }
};

/**
 * Helper methods for encapsulating command logic
 */
const commandLogic = {
    date(context, label, value) {
        const selector = formGroupSelector.date(label);
        context.api.useXpath();
        context.click(selector);
        const daySelector = formGroupSelector.dateDay(value);
        context.waitForElementVisible(daySelector);
        context.click(daySelector);
        context.api.useCss();
        return context;
    },
    radio(context, label, value) {
        const selector = formGroupSelector.radio(label, value);
        context.api.useXpath();
        context.click(selector);
        context.api.useCss();
        return context;
    },
    text(context, label, value) {
        const selector = formGroupSelector.text(label);
        context.api.useXpath();
        context.setValue(selector, value);
        context.api.useCss();
        return context;
    },
    textarea(context, label, value) {
        const selector = formGroupSelector.textarea(label);
        context.api.useXpath();
        context.setValue(selector, value);
        context.api.useCss();
        return context;
    },
    select(context, label, value) {
        const selector = formGroupSelector.select(label);
        context.api.useXpath();
        context.click(selector);
        const optionSelector = formGroupSelector.selectOption(label, value);
        context.waitForElementVisible(optionSelector);
        context.click(optionSelector);
        context.api.useCss();
        return context;
    },
    multiselect(context, label, value) {
        const selector = formGroupSelector.multiselect(label);
        context.api.useXpath();
        value.forEach(v => {
            context.click(selector);
            const optionSelector = formGroupSelector.multiselectOption(
                label,
                v
            );
            context.waitForElementVisible(optionSelector);
            context.click(optionSelector);
        });
        context.api.useCss();
        return context;
    }
};

const verifyCommandLogic = {
    _verifyValue(context, label, value, selector) {
        context.api.useXpath();
        context.api.getValue(selector, function(result) {
            this.assert.equal(
                result.value,
                value,
                `Validating '${label}' equals '${value}'`
            );
        });
        context.api.useCss();
        return context;
    },
    date(context, label, value) {
        const selector = verifyFormGroupSelector.date(label);
        const today = new Date();
        const month = today.getMonth() + 1;
        const year = today.getFullYear();
        const dateStr = `${month}/${value}/${year}`;
        return this._verifyValue(context, label, dateStr, selector);
    },
    radio(context, label, value) {
        const selector = verifyFormGroupSelector.radio(label);
        return this._verifyValue(context, label, value, selector);
    },
    text(context, label, value) {
        const selector = verifyFormGroupSelector.text(label);
        return this._verifyValue(context, label, value, selector);
    },
    textarea(context, label, value) {
        const selector = verifyFormGroupSelector.textarea(label);
        return this._verifyValue(context, label, value, selector);
    },
    select(context, label, value) {
        const selector = verifyFormGroupSelector.select(label);
        return this._verifyValue(context, label, value, selector);
    },
    multiselect(context, label, value) {
        const selector = verifyFormGroupSelector.multiselect(label);
        const valueStr = value.join(", ");
        return this._verifyValue(context, label, valueStr, selector);
    }
};

/**
 * Helper methods for creating objects that describe the fields used on a form group
 */
const fieldFactory = {
    date: label => ({
        type: "date",
        label
    }),
    radio: label => ({
        type: "radio",
        label
    }),
    text: label => ({
        type: "text",
        label
    }),
    textarea: label => ({
        type: "textarea",
        label
    }),
    select: label => ({
        type: "select",
        label
    }),
    multiselect: label => ({
        type: "multiselect",
        label
    })
};

/**
 * Helper methods for creating functions that can be used as commands
 */
const commandFactory = {
    date(label) {
        return function(value = "1") {
            return commandLogic.date(this, label, value);
        };
    },
    radio(label) {
        return function(value) {
            return commandLogic.radio(this, label, value);
        };
    },
    text(label) {
        return function(value) {
            return commandLogic.text(this, label, value);
        };
    },
    textarea(label) {
        return function(value) {
            return commandLogic.textarea(this, label, value);
        };
    },
    select(label) {
        return function(value) {
            return commandLogic.select(this, label, value);
        };
    },
    multiselect(labels) {
        return function(value) {
            return commandLogic.multiselect(this, labels, value);
        };
    }
};

const verifyCommandFactory = {
    date(label) {
        return function(value = "1") {
            return verifyCommandLogic.date(this, label, value);
        };
    },
    radio(label) {
        return function(value) {
            return verifyCommandLogic.radio(this, label, value);
        };
    },
    textarea(label) {
        return function(value) {
            return verifyCommandLogic.textarea(this, label, value);
        };
    },
    select(label) {
        return function(value) {
            return verifyCommandLogic.select(this, label, value);
        };
    },
    text(label) {
        return function(value) {
            return verifyCommandLogic.text(this, label, value);
        };
    },
    multiselect(labels) {
        return function(value) {
            return verifyCommandLogic.multiselect(this, labels, value);
        };
    }
};

module.exports = {
    fieldFactory,
    getFieldCommands: fields => {
        const commands = {};
        fields.forEach(field => {
            commands[field.label] = commandFactory[field.type](field.label);
            if (field.type in verifyCommandFactory) {
                commands["_" + field.label] = verifyCommandFactory[field.type](
                    field.label
                );
            }
        });
        return commands;
    }
};
