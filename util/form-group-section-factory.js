const { getFieldCommands } = require("./form-group-fields");
const { activeXpath } = require("./../util");

module.exports = ({ title, commands = {}, fields = {}, elements = {} }) => {
    const sharedElements = {
        accordionHeader: activeXpath(
            `//h3[contains(@class, 'slds-accordion__summary-heading')]//span[@title='${title}']`
        ),
        accordionHeaderParent: activeXpath(
            `//h3[contains(@class, 'slds-accordion__summary-heading')]//span[@title='${title}']/..`
        )
    };
    const sharedCommands = {
        waitForLoad() {
            return this.waitForElementVisible("@accordionHeader");
        },
        open() {
            let offsetTop;
            this.click("@accordionHeader");
            this.getAttribute(
                "@accordionHeaderParent",
                "offsetTop",
                result => (offsetTop = result.value)
            );
            this.api.perform((client, done) => {
                client.execute(
                    function(offsetTop) {
                        var offset =
                            document.querySelector(".stage").offsetTop +
                            parseInt(offsetTop, 10);
                        window.scrollTo(0, offset);
                    },
                    [offsetTop],
                    done
                );
            });
            return this;
        },
        set(label, value) {
            if (value === undefined) {
                value = label;
            }
            if (!(label in this)) {
                throw new Error(
                    `Unknown 'set' command "${label}".  Did you define the field for this label?`
                );
            }
            return this[label](value);
        },
        validate() {
            console.log(`Skipped validate for form group: '${title}'`);
        },
        check(label, value) {
            if (value === undefined) {
                value = label;
            }
            if (!(label in this)) {
                throw new Error(
                    `Unknown 'check' command "${label}".  Did you define the field for this label?`
                );
            }
            return this["_" + label](value);
        }
    };
    const fieldCommands = getFieldCommands(fields);
    commands = [Object.assign(sharedCommands, fieldCommands, commands)];
    elements = Object.assign(elements, sharedElements);
    return {
        commands,
        locateStrategy: "xpath",
        selector: `//h3[contains(@class, 'slds-accordion__summary-heading')]//span[@title='${title}']/ancestor::li`,
        elements
    };
};
