const { login } = require("./../config.json");



module.exports = {
  tags: ['example'],
  before(client) {
      client.maximizeWindow();
  },

  after(client) {
      client.end();
  },

  "Login Test": client => {
      client.testLogin(login.tester);
  },
};
