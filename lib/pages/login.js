const { css } = require("../util");

const commands = [
    {
        waitForLoad() {
            this.api.useCss();
            this.waitForElementVisible("#main-nav");
            this.api.useXpath();
            return this;
        },   

        popUpRegistration(loginForm) {
            return this.click()
        },
        setUsername(username) {
            return this.setValue("@username", username);
        },
        setPassword(password) {
            return this.setValue("@password", password);
        },
        login() {
            return this.waitForElementVisible("@submitButton")
                .click('@submitButton')
                .waitForElementNotPresent("@submitButton");
        }
    }
];

module.exports = {
    commands,
    url: "http://www.way2automation.com/demo.html",
    elements: {
        submitButton : css("#Login"),
        username:  css("input.username"),
        password: css("input.password")
        }
}