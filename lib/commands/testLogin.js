
module.exports.command = function testLogin(user) {
    //if (hasCookies(user)) {
        //return this.restoreLogin(user);
    //}
    const loginPage = this.page.login();
    loginPage
        .navigate()
        .waitForLoad()
        .setUsername(user.username)
        .setPassword(user.password)
        .login()
        .pause(5000);
    //this.getCookies(setCookies.bind(null, user));
    return this;
};