const xpath = selector => ({
    selector,
    locateStrategy: "xpath"
});

const activeXpath = selector => ({
    selector: activeXpathSelector(selector),
    locateStrategy: "xpath"
});

const activeXpathSelector = selector =>
    `//div[contains(@class, 'active')]${selector}`;

const css = selector => ({
    selector,
    locateStrategy: "css selector"
});

const lowerCaseFirstLetter = string =>
    string.charAt(0).toLowerCase() + string.slice(1);

const getPageObjectNameFromType = type => {
    type = type.replace(/[^a-zA-Z0-9]+/g, "");
    type = lowerCaseFirstLetter(type);
    return type;
};

module.exports = {
    activeXpath,
    activeXpathSelector,
    xpath,
    css,
    getPageObjectNameFromType
};


